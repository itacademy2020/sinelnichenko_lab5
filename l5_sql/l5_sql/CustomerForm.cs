﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace l5_sql
{
    public partial class CustomerForm : Form
    {
        string sqlconnectionstr = "Server=tcp:itacademy.database.windows.net,1433;Database=Synelchenko;User ID = synelchenko;Password=Ciio95113;Trusted_Connection=False;Encrypt=True;";

        public CustomerForm()
        {
            InitializeComponent();
        }

        private void btnEditCustomer_Click(object sender, EventArgs e)
        {
            SqlConnection connectionDB = new SqlConnection(sqlconnectionstr);
            if (txtEditAddress.Text == "" || txtEditContacts.Text == "" || txtEditName.Text == "")
            {
                MessageBox.Show("Заповніть всі поля!");
            }
            else
            {

            
                try
                {
                    if (comboEdit.SelectedItem == null)
                    {
                        MessageBox.Show("Оберіть покупця!");
                    }
                    else
                    {
                        Customer customer = (Customer)comboEdit.SelectedItem;

                        connectionDB.Open();

                        SqlCommand editNameCommand = new SqlCommand("UPDATE [dbo].[Customer] SET PIB = '" + txtEditName.Text.ToString() + "' where id =" + customer.id.ToString(), connectionDB);
                        editNameCommand.ExecuteNonQuery();

                        SqlCommand editContactsCommand = new SqlCommand("UPDATE [dbo].[Customer] SET Contacts = " + txtEditContacts.Text.ToString() + " where id =" + customer.id.ToString(), connectionDB);
                        editContactsCommand.ExecuteNonQuery();

                        SqlCommand editAddressCommand = new SqlCommand("UPDATE [dbo].[Customer] SET Address = '" + txtEditAddress.Text.ToString() + "' where id =" + customer.id.ToString(), connectionDB);
                        editAddressCommand.ExecuteNonQuery();


                        MessageBox.Show("Покупець відредагований успішно");
                    }
                }

                catch (Exception errorUpdate)
                {
                    MessageBox.Show("Телефон введено не вірно!");
                }
                finally
                {
                    connectionDB.Close();
                }
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void CustomerForm_Load(object sender, EventArgs e)
        {
            SqlConnection connectionDB = new SqlConnection(sqlconnectionstr);
            connectionDB.Open();
            SqlDataAdapter DA = new SqlDataAdapter("select * from [dbo].[Customer]", connectionDB);
            SqlDataReader datareaderManager = DA.SelectCommand.ExecuteReader();
            comboSelectCustomer.Items.Clear();

            while (datareaderManager.Read())
            {
                Customer customer = new Customer();
                customer.id = (int)datareaderManager[0];
                customer.PIB = datareaderManager[1].ToString();
                comboSelectCustomer.Items.Add(customer);
                comboSelectCustomer.DisplayMember = "PIB";
            }
            datareaderManager.Close();


            DA.SelectCommand = new SqlCommand("select * from [dbo].[Customer]", connectionDB);
            datareaderManager = DA.SelectCommand.ExecuteReader();
            comboEdit.Items.Clear();

            while (datareaderManager.Read())
            {
                Customer customer = new Customer();
                customer.id = (int)datareaderManager[0];
                customer.PIB = datareaderManager[1].ToString();
                comboEdit.Items.Add(customer);
                comboEdit.DisplayMember = "PIB";
            }
            connectionDB.Close();
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void btnSelectCustomer_Click(object sender, EventArgs e)
        {
            Customer customer = (Customer)comboSelectCustomer.SelectedItem;

            SqlConnection connection = new SqlConnection(sqlconnectionstr);
            if (comboSelectCustomer.SelectedItem == null)
            {
                MessageBox.Show("Оберіть покупця!");
            }
            else
            {
                connection.Open();

                SqlDataAdapter dataAdapter = new SqlDataAdapter("select * from [dbo].[Customer] where Customer.id =" + customer.id.ToString(), connection);

                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet, "Customer_Output");
                gridResult.DataSource = dataSet.Tables["Customer_Output"];
                gridResult.Refresh();


                connection.Close();
            }
        }

        private void btnAddCustomer_Click(object sender, EventArgs e)
        {
            SqlConnection connectionDB = new SqlConnection(sqlconnectionstr);
            if (txtAddName.Text == "" || txtAddContacts.Text == "" || txtAddAddress.Text == "")
            {
                MessageBox.Show("Заповніть всі поля!");
            }
            else
            {

           
                try
                {
                    connectionDB.Open();

                    SqlCommand addCommand = new SqlCommand("Insert into [dbo].[Customer] ([PIB],[Contacts],[Address]) values ('" + txtAddName.Text.ToString() + "', " + txtAddContacts.Text.ToString() + ",'" + txtAddAddress.Text.ToString() + "')", connectionDB);
                    addCommand.ExecuteNonQuery();
                    MessageBox.Show("Покупець доданий успішно");

                }
                catch (Exception err)
                {
                    MessageBox.Show("Телефон введено не вірно!");
                }
                finally
                {
                    connectionDB.Close();
                }
            }
        }
    }
}
