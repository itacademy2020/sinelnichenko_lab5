﻿namespace l5_sql
{
    partial class Main
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.getTask2 = new System.Windows.Forms.Button();
            this.getTask1 = new System.Windows.Forms.Button();
            this.getTask3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnManager = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.btnManager);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.getTask2);
            this.panel1.Controls.Add(this.getTask1);
            this.panel1.Controls.Add(this.getTask3);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(776, 210);
            this.panel1.TabIndex = 0;
            // 
            // getTask2
            // 
            this.getTask2.Location = new System.Drawing.Point(651, 72);
            this.getTask2.Name = "getTask2";
            this.getTask2.Size = new System.Drawing.Size(122, 62);
            this.getTask2.TabIndex = 3;
            this.getTask2.Text = "Task2";
            this.getTask2.UseVisualStyleBackColor = true;
            this.getTask2.Click += new System.EventHandler(this.getTask2_Click);
            // 
            // getTask1
            // 
            this.getTask1.Location = new System.Drawing.Point(651, 4);
            this.getTask1.Name = "getTask1";
            this.getTask1.Size = new System.Drawing.Size(122, 62);
            this.getTask1.TabIndex = 2;
            this.getTask1.Text = "Task1";
            this.getTask1.UseVisualStyleBackColor = true;
            this.getTask1.Click += new System.EventHandler(this.getTask1_Click);
            // 
            // getTask3
            // 
            this.getTask3.Location = new System.Drawing.Point(651, 140);
            this.getTask3.Name = "getTask3";
            this.getTask3.Size = new System.Drawing.Size(122, 62);
            this.getTask3.TabIndex = 1;
            this.getTask3.Text = "Task 3";
            this.getTask3.UseVisualStyleBackColor = true;
            this.getTask3.Click += new System.EventHandler(this.getTask3_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(3, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(152, 87);
            this.button1.TabIndex = 4;
            this.button1.Text = "Таблиця покупців";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnManager
            // 
            this.btnManager.Location = new System.Drawing.Point(174, 4);
            this.btnManager.Name = "btnManager";
            this.btnManager.Size = new System.Drawing.Size(152, 87);
            this.btnManager.TabIndex = 5;
            this.btnManager.Text = "Таблиця менеджерів";
            this.btnManager.UseVisualStyleBackColor = true;
            this.btnManager.Click += new System.EventHandler(this.btnManager_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(3, 115);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(152, 86);
            this.button3.TabIndex = 6;
            this.button3.Text = "Компанія доставки";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(174, 115);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(152, 87);
            this.button4.TabIndex = 7;
            this.button4.Text = "Таблиця каналів";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 241);
            this.Controls.Add(this.panel1);
            this.Name = "Main";
            this.Text = "Main";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button getTask3;
        private System.Windows.Forms.Button getTask2;
        private System.Windows.Forms.Button getTask1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnManager;
        private System.Windows.Forms.Button button1;
    }
}

