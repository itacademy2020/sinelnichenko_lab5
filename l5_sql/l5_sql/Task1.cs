﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace l5_sql
{
    public partial class Task1 : Form
    {
        string sqlconnectionstr = "Server=tcp:itacademy.database.windows.net,1433;Database=Synelchenko;User ID = synelchenko;Password=Ciio95113;Trusted_Connection=False;Encrypt=True;";

        public Task1()
        {
            InitializeComponent();
        }

        private void Task1_Load(object sender, EventArgs e)
        {
            SqlConnection connection = new SqlConnection(sqlconnectionstr);
            connection.Open();
            SqlDataAdapter dataAdapter = new SqlDataAdapter("SELECT [Name] FROM [dbo].[Product] WHERE[Name] NOT IN( SELECT[dbo].[Product].Name FROM[dbo].[Complaints] INNER JOIN[dbo].[Product] ON[dbo].[Product].Name = [dbo].[Complaints].Product_Name WHERE[dbo].[Complaints].Date_Create BETWEEN '2020-10-01' AND '2020-10-31')", connection);

            DataSet dataSet = new DataSet();
            dataAdapter.Fill(dataSet, "Product_list");
            gridResult.DataSource = dataSet.Tables["Product_list"];
            gridResult.Refresh();
            connection.Close();
        }
    }
}
