﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace l5_sql
{

    public partial class ChanelForm : Form
    {
        string sqlconnectionstr = "Server=tcp:itacademy.database.windows.net,1433;Database=Synelchenko;User ID = synelchenko;Password=Ciio95113;Trusted_Connection=False;Encrypt=True;";

        public ChanelForm()
        {
            InitializeComponent();
        }

        private void ChanelForm_Load(object sender, EventArgs e)
        {
            SqlConnection connectionDB = new SqlConnection(sqlconnectionstr);
            connectionDB.Open();
            SqlDataAdapter DA = new SqlDataAdapter("select * from [dbo].[Chanel]", connectionDB);
            SqlDataReader datareaderManager = DA.SelectCommand.ExecuteReader();
            comboChanelName.Items.Clear();

            while (datareaderManager.Read())
            {
                Chanel chanel = new Chanel();
                chanel.id = (int)datareaderManager[0];
                chanel.name = datareaderManager[1].ToString();
                comboChanelName.Items.Add(chanel);
                comboChanelName.DisplayMember = "name";
            }
            datareaderManager.Close();


            DA.SelectCommand = new SqlCommand("select * from [dbo].[Chanel]", connectionDB);
            datareaderManager = DA.SelectCommand.ExecuteReader();
            comboEditName.Items.Clear();

            while (datareaderManager.Read())
            {
                Chanel chanel = new Chanel();
                chanel.id = (int)datareaderManager[0];
                chanel.name = datareaderManager[1].ToString();
                comboEditName.Items.Add(chanel);
                comboEditName.DisplayMember = "name";
            }
            connectionDB.Close();
        }

        private void btnSelectChabel_Click(object sender, EventArgs e)
        {
            Chanel chanel = (Chanel)comboChanelName.SelectedItem;

            SqlConnection connection = new SqlConnection(sqlconnectionstr);
            if (comboChanelName.SelectedItem == null)
            {
                MessageBox.Show("Оберіть канал!");
            }
            else
            {
                connection.Open();

                SqlDataAdapter dataAdapter = new SqlDataAdapter("select * from [dbo].[Chanel] where Chanel.id =" + chanel.id.ToString(), connection);

                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet, "Chanel_Output");
                gridResult.DataSource = dataSet.Tables["Chanel_Output"];
                gridResult.Refresh();


                connection.Close();
            }
        }

        private void btnAddName_Click(object sender, EventArgs e)
        {
            SqlConnection connectionDB = new SqlConnection(sqlconnectionstr);
            if (txtAddName.Text == "")
            {
                MessageBox.Show("Заповніть поле!");
            }
            else
            {
                try
                {
                    connectionDB.Open();

                    SqlCommand addCommand = new SqlCommand("Insert into [dbo].[Chanel] ([Name]) values ('" + txtAddName.Text.ToString() + "')", connectionDB);
                    addCommand.ExecuteNonQuery();
                    MessageBox.Show("Канал доданий успішно");

                }
                catch (Exception err)
                {
                    MessageBox.Show("Введено неправильний формат даних!");
                }
                finally
                {
                    connectionDB.Close();
                }
            }
        }

        private void btnEditName_Click(object sender, EventArgs e)
        {
            SqlConnection connectionDB = new SqlConnection(sqlconnectionstr);
            if (txtEditName.Text == "")
            {
                MessageBox.Show("Заповніть поле! ");
            }
            else
            {


                try
                {
                    if (comboEditName.SelectedItem == null)
                    {
                        MessageBox.Show("Оберіть канал!");
                    }
                    else
                    {
                        Chanel chanel = (Chanel)comboEditName.SelectedItem;
                        connectionDB.Open();
                        SqlCommand editNameCommand = new SqlCommand("UPDATE [dbo].[Chanel] SET Name = '" + txtEditName.Text.ToString() + "' where id =" + chanel.id.ToString(), connectionDB);
                        editNameCommand.ExecuteNonQuery();
                        MessageBox.Show("Менеджер відредагований успішно");
                    }
                }

                catch (Exception errorUpdate)
                {
                    MessageBox.Show("Введено неправильний формат даних!");
                }
                finally
                {
                    connectionDB.Close();
                }
            }
        
    }
    }
}
