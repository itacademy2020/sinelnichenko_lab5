﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace l5_sql
{
    public partial class ManagerForm : Form
    {
        string sqlconnectionstr = "Server=tcp:itacademy.database.windows.net,1433;Database=Synelchenko;User ID = synelchenko;Password=Ciio95113;Trusted_Connection=False;Encrypt=True;";

        //private bool ValidContacts(string phone)
        //{
        //    //Regex regex = new Regex(@"^\d$");
        //    string regexPattern = @"^\d$";
        //    return Regex.IsMatch(phone, regexPattern);
        //}
        public ManagerForm()
        {
            InitializeComponent();
        }

        private void ManagerForm_Load(object sender, EventArgs e)
        {
            SqlConnection connectionDB = new SqlConnection(sqlconnectionstr);
            connectionDB.Open();
            SqlDataAdapter DA = new SqlDataAdapter("select * from [dbo].[Manager]", connectionDB);
            SqlDataReader datareaderManager = DA.SelectCommand.ExecuteReader();
            comboOutput.Items.Clear();

            while (datareaderManager.Read())
            {
                Manager mg = new Manager();
                mg.id = (int)datareaderManager[0];
                mg.PIB = datareaderManager[1].ToString();
                comboOutput.Items.Add(mg);
                comboOutput.DisplayMember = "PIB";
            }
            datareaderManager.Close();


            DA.SelectCommand = new SqlCommand("select * from [dbo].[Manager]", connectionDB);
            datareaderManager = DA.SelectCommand.ExecuteReader();
            comboDrop.Items.Clear();

            while (datareaderManager.Read())
            {
                Manager mg = new Manager();
                mg.id = (int)datareaderManager[0];
                mg.PIB = datareaderManager[1].ToString();
                comboDrop.Items.Add(mg);
                comboDrop.DisplayMember = "PIB";
            }
            connectionDB.Close();

        }

        private void btnOutput_Click(object sender, EventArgs e)
        {
            Manager manager = (Manager)comboOutput.SelectedItem;

            SqlConnection connection = new SqlConnection(sqlconnectionstr);
            if (comboOutput.SelectedItem == null)
            {
                MessageBox.Show("Оберіть менеджера!");
            }
            else
            {
                connection.Open();

                SqlDataAdapter dataAdapter = new SqlDataAdapter("select * from [dbo].[Manager] where Manager.id =" + manager.id.ToString(), connection);

                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet, "Manager_Output");
                gridResult.DataSource = dataSet.Tables["Manager_Output"];
                gridResult.Refresh();


                connection.Close();
            }
            
        }

        private void btnInput_Click(object sender, EventArgs e)
        {
            SqlConnection connectionDB = new SqlConnection(sqlconnectionstr);
            if (txtName.Text == "" || txtContacts.Text == "")
            {
                MessageBox.Show("Заповніть всі поля");
            }
            else
            {
                try
                {
                    connectionDB.Open();

                    SqlCommand addCommand = new SqlCommand("Insert into [dbo].[Manager] ([PIB],[Contacts]) values ('" + txtName.Text.ToString() + "', " + txtContacts.Text.ToString() + ")", connectionDB);
                    addCommand.ExecuteNonQuery();
                    MessageBox.Show("Менеджер доданий успішно");

                }
                catch (Exception err)
                {
                    MessageBox.Show("Телефон введено невірно!");
                }
                finally
                {
                    connectionDB.Close();
                }
            }
        }

        private void comDrop_SelectedIndexChanged(object sender, EventArgs e)
        {
            

        }

        private void btnDrop_Click(object sender, EventArgs e)
        {
            SqlConnection connectionDB = new SqlConnection(sqlconnectionstr);
            if (txtEditName.Text == "" || txtEditContacts.Text == "")
            {
                MessageBox.Show("Заповніть всі поля");
            }
            else
            {
                try
                {
                    if (comboDrop.SelectedItem == null)
                    {
                        MessageBox.Show("Оберіть менеджера!");
                    }
                    else
                    {
                        Manager manager = (Manager)comboDrop.SelectedItem;

                        connectionDB.Open();
                        
                            SqlCommand editNameCommand = new SqlCommand("UPDATE [dbo].[Manager] SET PIB = '" + txtEditName.Text.ToString() + "' where id =" + manager.id.ToString(), connectionDB);
                            editNameCommand.ExecuteNonQuery();
                       
                            SqlCommand editContactsCommand = new SqlCommand("UPDATE [dbo].[Manager] SET Contacts = " + txtEditContacts.Text.ToString() + " where id =" + manager.id.ToString(), connectionDB);
                            editContactsCommand.ExecuteNonQuery();
                        

                        MessageBox.Show("Менеджер відредагований успішно");
                    }
                }

                catch (Exception errorUpdate)
                {
                    MessageBox.Show("Телефон введено невірно!");
                }
                finally
                {
                    connectionDB.Close();
                }
            }
        }

        private void txtContacts_TextChanged(object sender, EventArgs e)
        {
            //if (!ValidContacts(txtContacts.Text))
            //{
            //    e.Cancel = true;
            //}
        }
    }
}

