﻿namespace l5_sql
{
    partial class DeliveryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.comboAddManager = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAddCompany = new System.Windows.Forms.TextBox();
            this.btnAddCompany = new System.Windows.Forms.Button();
            this.btnSelectCompany = new System.Windows.Forms.Button();
            this.btnEditCompany = new System.Windows.Forms.Button();
            this.comboSelectCompany = new System.Windows.Forms.ComboBox();
            this.comboEditCompany = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboEditManager = new System.Windows.Forms.ComboBox();
            this.txtEditCompany = new System.Windows.Forms.TextBox();
            this.gridResult = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridResult)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Location = new System.Drawing.Point(3, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(365, 472);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.gridResult);
            this.panel2.Location = new System.Drawing.Point(374, 15);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(759, 469);
            this.panel2.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnAddCompany);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtAddCompany);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.comboAddManager);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(359, 146);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Додати компанію";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboSelectCompany);
            this.groupBox2.Controls.Add(this.btnSelectCompany);
            this.groupBox2.Location = new System.Drawing.Point(3, 155);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(359, 115);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Вивести компанію";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.btnEditCompany);
            this.groupBox3.Controls.Add(this.txtEditCompany);
            this.groupBox3.Controls.Add(this.comboEditManager);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.comboEditCompany);
            this.groupBox3.Location = new System.Drawing.Point(3, 276);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(359, 186);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Редагування компанії";
            // 
            // comboAddManager
            // 
            this.comboAddManager.FormattingEnabled = true;
            this.comboAddManager.Location = new System.Drawing.Point(6, 25);
            this.comboAddManager.Name = "comboAddManager";
            this.comboAddManager.Size = new System.Drawing.Size(179, 28);
            this.comboAddManager.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(191, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(160, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Оберіть менеджера";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(191, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Назва компанії";
            // 
            // txtAddCompany
            // 
            this.txtAddCompany.Location = new System.Drawing.Point(6, 58);
            this.txtAddCompany.Name = "txtAddCompany";
            this.txtAddCompany.Size = new System.Drawing.Size(179, 26);
            this.txtAddCompany.TabIndex = 3;
            // 
            // btnAddCompany
            // 
            this.btnAddCompany.Location = new System.Drawing.Point(6, 90);
            this.btnAddCompany.Name = "btnAddCompany";
            this.btnAddCompany.Size = new System.Drawing.Size(179, 50);
            this.btnAddCompany.TabIndex = 2;
            this.btnAddCompany.Text = "Додати";
            this.btnAddCompany.UseVisualStyleBackColor = true;
            this.btnAddCompany.Click += new System.EventHandler(this.btnAddCompany_Click);
            // 
            // btnSelectCompany
            // 
            this.btnSelectCompany.Location = new System.Drawing.Point(6, 59);
            this.btnSelectCompany.Name = "btnSelectCompany";
            this.btnSelectCompany.Size = new System.Drawing.Size(179, 50);
            this.btnSelectCompany.TabIndex = 2;
            this.btnSelectCompany.Text = "Вивести";
            this.btnSelectCompany.UseVisualStyleBackColor = true;
            this.btnSelectCompany.Click += new System.EventHandler(this.btnSelectCompany_Click);
            // 
            // btnEditCompany
            // 
            this.btnEditCompany.Location = new System.Drawing.Point(6, 125);
            this.btnEditCompany.Name = "btnEditCompany";
            this.btnEditCompany.Size = new System.Drawing.Size(179, 50);
            this.btnEditCompany.TabIndex = 3;
            this.btnEditCompany.Text = "Редагувати";
            this.btnEditCompany.UseVisualStyleBackColor = true;
            this.btnEditCompany.Click += new System.EventHandler(this.button3_Click);
            // 
            // comboSelectCompany
            // 
            this.comboSelectCompany.FormattingEnabled = true;
            this.comboSelectCompany.Location = new System.Drawing.Point(6, 25);
            this.comboSelectCompany.Name = "comboSelectCompany";
            this.comboSelectCompany.Size = new System.Drawing.Size(179, 28);
            this.comboSelectCompany.TabIndex = 2;
            // 
            // comboEditCompany
            // 
            this.comboEditCompany.FormattingEnabled = true;
            this.comboEditCompany.Location = new System.Drawing.Point(6, 25);
            this.comboEditCompany.Name = "comboEditCompany";
            this.comboEditCompany.Size = new System.Drawing.Size(179, 28);
            this.comboEditCompany.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(191, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(143, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Оберіть компанію";
            // 
            // comboEditManager
            // 
            this.comboEditManager.FormattingEnabled = true;
            this.comboEditManager.Location = new System.Drawing.Point(6, 59);
            this.comboEditManager.Name = "comboEditManager";
            this.comboEditManager.Size = new System.Drawing.Size(179, 28);
            this.comboEditManager.TabIndex = 4;
            // 
            // txtEditCompany
            // 
            this.txtEditCompany.Location = new System.Drawing.Point(6, 93);
            this.txtEditCompany.Name = "txtEditCompany";
            this.txtEditCompany.Size = new System.Drawing.Size(179, 26);
            this.txtEditCompany.TabIndex = 4;
            // 
            // gridResult
            // 
            this.gridResult.AllowUserToAddRows = false;
            this.gridResult.AllowUserToDeleteRows = false;
            this.gridResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridResult.Location = new System.Drawing.Point(3, 3);
            this.gridResult.Name = "gridResult";
            this.gridResult.ReadOnly = true;
            this.gridResult.RowHeadersWidth = 62;
            this.gridResult.RowTemplate.Height = 28;
            this.gridResult.Size = new System.Drawing.Size(753, 462);
            this.gridResult.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(195, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(161, 20);
            this.label4.TabIndex = 1;
            this.label4.Text = "Нова назва компанії";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(195, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(160, 20);
            this.label5.TabIndex = 5;
            this.label5.Text = "Оберіть менеджера";
            // 
            // DeliveryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1145, 492);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "DeliveryForm";
            this.Text = "DeliveryForm";
            this.Load += new System.EventHandler(this.DeliveryForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridResult)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnAddCompany;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtAddCompany;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboAddManager;
        private System.Windows.Forms.Button btnEditCompany;
        private System.Windows.Forms.Button btnSelectCompany;
        private System.Windows.Forms.ComboBox comboSelectCompany;
        private System.Windows.Forms.TextBox txtEditCompany;
        private System.Windows.Forms.ComboBox comboEditManager;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboEditCompany;
        private System.Windows.Forms.DataGridView gridResult;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}