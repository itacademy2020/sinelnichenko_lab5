﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace l5_sql
{
    public partial class Task2 : Form
    {
        string sqlconnectionstr = "Server=tcp:itacademy.database.windows.net,1433;Database=Synelchenko;User ID = synelchenko;Password=Ciio95113;Trusted_Connection=False;Encrypt=True;";

        public Task2()
        {
            InitializeComponent();
        }

        private void Task2_Load(object sender, EventArgs e)
        {
            SqlConnection connectionDB = new SqlConnection(sqlconnectionstr);
            connectionDB.Open();

            SqlCommand selectProductType = new SqlCommand("select id, Product_Type from[dbo].[Product]", connectionDB);

            SqlDataReader datareaderProductType = selectProductType.ExecuteReader();


            comboProductType.Items.Clear();
            while (datareaderProductType.Read())
            {
                ProductType product = new ProductType();
                product.id = (int)datareaderProductType[0];
                product.name = datareaderProductType[1].ToString();
                comboProductType.Items.Add(product);
                comboProductType.DisplayMember = "name";
            }
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            ProductType product = (ProductType)comboProductType.SelectedItem;

            SqlConnection connection = new SqlConnection(sqlconnectionstr);
            if (comboProductType.SelectedItem == null)
            {
                MessageBox.Show("Тип продукту не був обраний! Обрався перший зі списку.");
                product = (ProductType)comboProductType.Items[0];
            }

            connection.Open();


            SqlDataAdapter dataAdapter = new SqlDataAdapter("select [dbo].[Customer].PIB, Address, [dbo].[Complaints].Date_Create, Date_Close, Delivery_name, Product_Type FROM[dbo].[Customer] inner join Complaints ON Customer.id = Complaints.Customer_id WHERE[dbo].[Complaints].Product_id =" + product.id.ToString(), connection);

            DataSet dataSet = new DataSet();
            dataAdapter.Fill(dataSet, "Customers_list");
            gridResult.DataSource = dataSet.Tables["Customers_list"];
            gridResult.Refresh();
            connection.Close();
        }

        private void comboProductType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ProductType product = (ProductType)comboProductType.SelectedItem;
            //MessageBox.Show(product.id.ToString());
        }
    }
}
