﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace l5_sql
{
    public partial class DeliveryForm : Form
    {
        string sqlconnectionstr = "Server=tcp:itacademy.database.windows.net,1433;Database=Synelchenko;User ID = synelchenko;Password=Ciio95113;Trusted_Connection=False;Encrypt=True;";

        public DeliveryForm()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SqlConnection connectionDB = new SqlConnection(sqlconnectionstr);
            if (txtEditCompany.Text == "")
            {
                MessageBox.Show("Заповніть всі поля");
            }
            else
            {
                try
                {

                    DeliveryCompany dc = (DeliveryCompany)comboEditManager.SelectedItem;
                    DeliveryCompany delco = (DeliveryCompany)comboEditCompany.SelectedItem;
                    connectionDB.Open();

                    SqlDataAdapter DA = new SqlDataAdapter($"UPDATE [dbo].[Delivery_Company] SET Name = '{txtEditCompany.Text}', Manager_id = '{dc.id}' where [dbo].[Delivery_Company].id =" + delco.id.ToString(), connectionDB);
                    DA.SelectCommand.ExecuteReader();

                    MessageBox.Show("Компанія змінена успішно");


                    MessageBox.Show("Заповніть усі поля!");

                }
                catch (Exception err)
                {
                    MessageBox.Show("Введено невірний формат даних!");
                }
                finally
                {
                    connectionDB.Close();
                }
            }
        }

        private void DeliveryForm_Load(object sender, EventArgs e)
        {
            SqlConnection connectionDB = new SqlConnection(sqlconnectionstr);
            connectionDB.Open();
            SqlDataAdapter DA = new SqlDataAdapter("select * from [dbo].[Delivery_Company]", connectionDB);
            SqlDataReader datareaderManager = DA.SelectCommand.ExecuteReader();
            comboSelectCompany.Items.Clear();

            while (datareaderManager.Read())
            {
                DeliveryCompany dc = new DeliveryCompany();
                dc.id = (int)datareaderManager[0];
                dc.name = datareaderManager[1].ToString();
                comboSelectCompany.Items.Add(dc);
                comboSelectCompany.DisplayMember = "name";
            }
            datareaderManager.Close();


            DA.SelectCommand = new SqlCommand("select * from [dbo].[Delivery_Company]", connectionDB);
            datareaderManager = DA.SelectCommand.ExecuteReader();
            comboEditCompany.Items.Clear();

            while (datareaderManager.Read())
            {
                DeliveryCompany dc = new DeliveryCompany();
                dc.id = (int)datareaderManager[0];
                dc.name = datareaderManager[1].ToString();
                comboEditCompany.Items.Add(dc);
                comboEditCompany.DisplayMember = "name";
            }
            datareaderManager.Close();

            DA.SelectCommand = new SqlCommand("select * from [dbo].[Manager]", connectionDB);
            datareaderManager = DA.SelectCommand.ExecuteReader();
            comboAddManager.Items.Clear();

            while (datareaderManager.Read())
            {
                DeliveryCompany dc = new DeliveryCompany();
                dc.id = (int)datareaderManager[0];
                dc.name = datareaderManager[1].ToString();
                comboAddManager.Items.Add(dc);
                comboAddManager.DisplayMember = "name";
            }
            datareaderManager.Close();

            DA.SelectCommand = new SqlCommand("select * from [dbo].[Manager]", connectionDB);
            datareaderManager = DA.SelectCommand.ExecuteReader();
            comboEditManager.Items.Clear();

            while (datareaderManager.Read())
            {
                DeliveryCompany dc = new DeliveryCompany();
                dc.id = (int)datareaderManager[0];
                dc.name = datareaderManager[1].ToString();
                comboEditManager.Items.Add(dc);
                comboEditManager.DisplayMember = "name";
            }
            connectionDB.Close();
        }

        private void btnSelectCompany_Click(object sender, EventArgs e)
        {
            DeliveryCompany manager = (DeliveryCompany)comboSelectCompany.SelectedItem;

            SqlConnection connection = new SqlConnection(sqlconnectionstr);
            if (comboSelectCompany.SelectedItem == null)
            {
                MessageBox.Show("Оберіть менеджера!");
            }
            else
            {
                connection.Open();

                SqlDataAdapter dataAdapter = new SqlDataAdapter("select * from [dbo].[Delivery_Company] where Delivery_Company.id =" + manager.id.ToString(), connection);

                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet, "DeliveryCompany_Output");
                gridResult.DataSource = dataSet.Tables["DeliveryCompany_Output"];
                gridResult.Refresh();


                connection.Close();
            }
        }

        private void btnAddCompany_Click(object sender, EventArgs e)
        {
            SqlConnection connectionDB = new SqlConnection(sqlconnectionstr);
            if (txtAddCompany.Text == "")
            {
                MessageBox.Show("Заповніть всі поля");
            }
            else
            {
                try
                {
                    DeliveryCompany dc = (DeliveryCompany)comboAddManager.SelectedItem;
                    connectionDB.Open();

                    SqlDataAdapter DA = new SqlDataAdapter($"INSERT INTO[dbo].[Delivery_Company] ([Name],[Manager_id]) VALUES('{txtAddCompany.Text}', '{dc.id}')", connectionDB);
                    DA.SelectCommand.ExecuteReader();

                    MessageBox.Show("Компанія додана успішно");

                }
                catch (Exception err)
                {
                    MessageBox.Show("Введено невірний формат даних!");
                }
                finally
                {
                    connectionDB.Close();
                }
            }
        }
        
    }
}
