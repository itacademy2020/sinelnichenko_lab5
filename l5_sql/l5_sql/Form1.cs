﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace l5_sql
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void getTask3_Click(object sender, EventArgs e)
        {
            Task3 task3 = new Task3();
            task3.ShowDialog();
        }

        private void getTask2_Click(object sender, EventArgs e)
        {
            Task2 task2 = new Task2();
            task2.ShowDialog();
        }

        private void getTask1_Click(object sender, EventArgs e)
        {
            Task1 task1 = new Task1();
            task1.ShowDialog();
        }

        private void btnManager_Click(object sender, EventArgs e)
        {
            ManagerForm managerform = new ManagerForm();
            managerform.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ChanelForm chanelform = new ChanelForm();
            chanelform.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CustomerForm customerform = new CustomerForm();
            customerform.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DeliveryForm deliveryform = new DeliveryForm();
            deliveryform.ShowDialog();
        }
    }
}
