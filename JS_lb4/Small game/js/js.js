// useful to have them as global variables
let canvas, ctx, w, h, paintLvl; 
let mousePos;


let playerName = 'user';
setTimeout(() => {
  let playerNameInput = document.querySelector('.name');
  playerNameInput.addEventListener("change", (event) => {
    playerName = event.target.value;});}, 100);


let playerSize = 20;
setTimeout(() => {
  let playerSizeInput = document.querySelector('.playerSizes');
  playerSizeInput.addEventListener("change", (event) => {
   playerSize = event.target.value;});},100);


let speedBalls = 1;
setTimeout(() => {
  let speedBallsInput = document.querySelector('.speedBalls');
  speedBallsInput.addEventListener("change", (event) => {
    speedBalls = event.target.value;});}, 100);


let maxSizeBalls = 30;
setTimeout(() => {
  let maxSizeBallsInput = document.querySelector('.maxSizeBalls');
  maxSizeBallsInput.addEventListener("change", (event) => {
    maxSizeBalls = event.target.value;});}, 100);


let minSizeBalls = 5;
setTimeout(() => {
  let minSizeBallsInput = document.querySelector('.minSizeBalls');
  minSizeBallsInput.addEventListener("change", (event) => {
    minSizeBalls = event.target.value;});}, 100);


let ballsForWin=0;
setTimeout(() => {
  let ballsForWinInput = document.querySelector('.valueForWin');
  ballsForWinInput.addEventListener("change", (event) => {  
  ballsForWin = event.target.value;});}, 100);

  let player = {
    x: 10,
    y: 10,
    width: 20,
    height: 20,
    color: 'red'
  }

let balls = []; 
let ballsValue = 10  ;
let lvlValue = 1;




//window.onload =
 function init() {

  player.width = +playerSize;
  player.height = +playerSize;
  
  // called AFTER the page has been loaded
  canvas = document.querySelector("#myCanvas");
  // often useful
  w = canvas.width; 
  h = canvas.height;  
  
  // important, we will draw with this object
  ctx = canvas.getContext('2d');
  paintLvl = canvas.getContext('2d');
  
  // create 10 balls
 
  balls = createBalls(ballsValue);
  
  
  // add a mousemove event listener to the canvas
  canvas.addEventListener('mousemove', mouseMoved);

  // ready to go !
  mainLoop();
  ballsValue++;
  lvlValue++;
  
   
};

function mouseMoved(evt) {
  mousePos = getMousePos(canvas, evt);
}

function getMousePos(canvas, evt) {
  // necessary work in the canvas coordinate system
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
}

function movePlayerWithMouse() {
  if(mousePos !== undefined) {
    player.x = mousePos.x;
    player.y = mousePos.y;
  }
}

function mainLoop() {
  
  // 1 - clear the canvas
  ctx.clearRect(0, 0, w, h);
  paintLvl.clearRect(0,0,w,h);
  
  // draw the ball and the player
  drawFilledRectangle(player);
  drawAllBalls(balls);
  drawNumberOfBallsAlive(balls);

  // animate the ball that is bouncing all over the walls
  moveAllBalls(balls);
  
  movePlayerWithMouse();
  
  // ask for a new animation frame
  requestAnimationFrame(mainLoop);
  

}

// Collisions between rectangle and circle
function circRectsOverlap(x0, y0, w0, h0, cx, cy, r) {
  let testX = cx;
  let testY = cy;
  if (testX < x0) testX = x0;
  if (testX > (x0 + w0)) testX = (x0 + w0);
  if (testY < y0) testY = y0;
  if (testY > (y0+h0)) testY = (y0 + h0);
  return (((cx - testX) * (cx - testX) + (cy - testY) * (cy - testY)) <  r * r);
}

function createBalls(n) {
  // empty array
  const ballArray = [];
  
  // create n balls
  for(let i=0; i < n; i++) {
    const b = {
      x: w/2,
      y: h/2,
      radius: (+minSizeBalls) + (+maxSizeBalls) * Math.random(), // between 5 and 35
      speedX: (-5 + 10 * Math.random()*speedBalls), // between -5 and + 5
      speedY: (-5 + 10 * Math.random()*speedBalls), // between -5 and + 5  
      color: getARandomColor(),
    }
    // add ball b to the array
     ballArray.push(b);
  }
  // returns the array full of randomly created balls
  return ballArray;
}

function getARandomColor() {
  const colors = ['red', 'blue', 'cyan', 'purple', 'pink', 'green', 'yellow'];
  // a value between 0 and color.length-1
  // Math.round = rounded value
  // Math.random() a value between 0 and 1
  let colorIndex = Math.round((colors.length-1) * Math.random()); 
  let c = colors[colorIndex];
  
  // return the random color
  return c;
}



function drawNumberOfBallsAlive(balls) {
  ctx.save();
  ctx.font="30px Arial";
  paintLvl.font="30px Arial";

  if(balls.length === +ballsForWin) {
    lvlValue++;
    init();
    ctx.fillText("YOU WIN!", 20, 30);
    
    
      $.post("http://localhost:3000/playerName",
      {UserName: playerName,lvl: lvlValue/2, });
    
    let scorePlayerObj = {
      name: playerName,
      lvl: lvlValue/2
    };
    let serialScoreObj = JSON.stringify(scorePlayerObj);
    localStorage.setItem(playerName,serialScoreObj);
    let returnScorePlayerObj = JSON.parse(localStorage.getItem(playerName));
    console.log(returnScorePlayerObj);
    
    

  } else {
    ctx.fillText(balls.length, 20, 30);
    paintLvl.fillText(lvlValue/2, 20, 70);
  }
  ctx.restore();
  
}

function drawAllBalls(ballArray) {
  ballArray.forEach(function(b) {
    drawFilledCircle(b);
  });
}

function moveAllBalls(ballArray) {
  // iterate on all balls in array
  ballArray.forEach(function(b, index) {
    // b is the current ball in the array
    b.x += b.speedX;
    b.y += b.speedY;
    testCollisionBallWithWalls(b); 
    testCollisionWithPlayer(b, index);
  });
}

function testCollisionWithPlayer(b, index) {
  if(circRectsOverlap(player.x, player.y,
                     player.width, player.height,
                     b.x, b.y, b.radius)) {
    // we remove the element located at index
    // from the balls array
    // splice: first parameter = starting index
    //         second parameter = number of elements to remove
    balls.splice(index, 1);
    // player.width--;
    // player.height--;
  }
}

function testCollisionBallWithWalls(b) {
  // COLLISION WITH VERTICAL WALLS ?
  if((b.x + b.radius) > w) {
    // the ball hit the right wall
    // change horizontal direction
    b.speedX = -b.speedX;
    
    // put the ball at the collision point
    b.x = w - b.radius;
  } else if((b.x -b.radius) < 0) {
    // the ball hit the left wall
    // change horizontal direction
    b.speedX = -b.speedX;
    
    // put the ball at the collision point
    b.x = b.radius;
  }
 
  // COLLISIONS WTH HORIZONTAL WALLS ?
  // Not in the else as the ball can touch both
  // vertical and horizontal walls in corners
  if((b.y + b.radius) > h) {
    // the ball hit the right wall
    // change horizontal direction
    b.speedY = -b.speedY;
    
    // put the ball at the collision point
    b.y = h - b.radius;
  } else if((b.y -b.radius) < 0) {
    // the ball hit the left wall
    // change horizontal direction
    b.speedY = -b.speedY;
    
    // put the ball at the collision point
    b.Y = b.radius;
  }  
}

function drawFilledRectangle(r) {
  // GOOD practice: save the context, use 2D trasnformations
  ctx.save();
  
  // translate the coordinate system, draw relative to it
  ctx.translate(r.x, r.y);
  
  ctx.fillStyle = r.color;
  // (0, 0) is the top left corner of the monster.
  ctx.fillRect(0, 0, r.width, r.height);
  
  // GOOD practice: restore the context
  ctx.restore();
}

function drawFilledCircle(c) {
  // GOOD practice: save the context, use 2D trasnformations
  ctx.save();
  
  // translate the coordinate system, draw relative to it
  ctx.translate(c.x, c.y);
  
  ctx.fillStyle = c.color;
  // (0, 0) is the top left corner
  ctx.beginPath();
  ctx.arc(0, 0, c.radius, 0, 2*Math.PI);
  ctx.fill();
 
  // GOOD practice: restore the context
  ctx.restore();
}

// function testF(){


// let playerObj={
//   name: playerName,
//   sizeplayer: playerSize,
//   ballsspeed: speedBalls,
//   maxsize: maxSizeBalls,
//   minsize: minSizeBalls,
//   valueofballs: ballsForWin
// };

// let serialObj = JSON.stringify(playerObj);
// localStorage.setItem(playerName,serialObj);
// let returnPlayerObj = JSON.parse(localStorage.getItem(playerName));

// console.log(returnPlayerObj);}



