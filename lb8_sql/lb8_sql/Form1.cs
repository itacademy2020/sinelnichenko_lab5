﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lb8_sql
{
    public partial class Form1 : Form
    {
        SynelchenkoEntities DB = new SynelchenkoEntities();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            dgvResults.DataSource = DB.Manager.ToList();
        }

        private void btnAddManager_Click(object sender, EventArgs e)
        {
            string contacts = txtContacts.Text;
            try
            {
                if ((string.IsNullOrWhiteSpace(txtName.Text) && txtName.Text.Length > 0) || (txtName.Text == "") || (txtName.Text == " "))
                {
                    MessageBox.Show("Введіть і'мя");
                    return;
                }
                if ((string.IsNullOrWhiteSpace(txtContacts.Text) && txtContacts.Text.Length > 0) || (txtContacts.Text == "") || (txtContacts.Text == " "))
                {
                    MessageBox.Show("Введіть телефон");
                    return;
                }
                else
                {
                    DB.Add_Manager(txtName.Text, Convert.ToInt64(contacts));
                    MessageBox.Show("Менеджер доданий успішно");
                }
            }
            catch (Exception error)
            {
                MessageBox.Show("Телефон введено невірно!");
            }
        }
    }
}
